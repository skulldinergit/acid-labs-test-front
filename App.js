

import { registerRootComponent} from 'expo';
import App from "./src/index";

export default registerRootComponent(App);