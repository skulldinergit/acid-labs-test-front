export default helpers = {
    checkIfPropertyIsArray: (property) => {
        if (property) {
            if (Array.isArray(property)) {
                return true;
            }
        }
    
        return false;
    }
}