import React from 'react';
import { View, ActivityIndicator, Dimensions } from 'react-native';
import { Overlay, Text, Divider, Icon } from 'react-native-elements';
import styles from './styles';

const {height, width} = Dimensions.get('window');

export default class ModalComponent extends React.Component {

    showLoading() {
        return (
            <View style={styles.loading}>
                <ActivityIndicator size="large" color="#0000ff" />
                <Text>Cargando ...</Text>
            </View>
        );
    }

    showError(error) {
        return (
            <View style={styles.error}>
                <Icon 
                    type='ionicon'
                    name='md-alert'
                    size={52}
                    color='#f4ce42'

                />
                <Text style={styles.errorTitle}>¡Error!</Text>
                <Text>{error}</Text>
                
            </View>
        );
    }

    render() {
        const { title, isVisible, children, onBackDropPress, loading, error } = this.props;

        return (
            <Overlay
                
                borderRadius={10}
                width={width-20}
                height={height-40}
                isVisible={isVisible}
                onBackdropPress={() => onBackDropPress()}
            >
                <View
                    style={styles.container}>
                    <View style={styles.header}>
                        <Text></Text>
                        <Text style={styles.title}>{title}</Text>
                        <Icon 
                            type='ionicon'
                            name='md-close'
                            onPress={(e) => onBackDropPress()}
                        />
                    </View>
                    
                    <Divider />
                    {loading && this.showLoading()}
                    {!loading && error === null && children}
                    {(!loading && error !== null) && this.showError(error)}
                </View>
            </Overlay>
        );
    }
}
