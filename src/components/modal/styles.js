import { StyleSheet } from 'react-native';

export default styles = StyleSheet.create({
    container: {
        paddingHorizontal: 20
    },
    title: {
        fontSize: 15,
        fontWeight: 'bold',
        color: 'gray'
    },
    header: {
        flexDirection: 'row', 
        justifyContent:'space-between', 
        marginRight:-10, 
        marginBottom: 10
    },
    loading: {
        flexDirection: 'column', 
        justifyContent: 'center', 
        alignItems: 'center', 
        marginTop:50
    },
    error: {
        flexDirection: 'column', 
        justifyContent: 'center', 
        alignItems: 'center', 
        marginTop:50
    },
    errorTitle: {
        fontWeight: 'bold',
        fontSize: 15,
        marginVertical: 10,
    }
});