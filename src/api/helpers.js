import helpers from '../utils/helpers';
import constants from './constants';
import countryCapitals from '../../assets/json/country-capitals.json';

export const mapHelper = {
    obtainCountryData: ({ results }) => {
        if (!helpers.checkIfPropertyIsArray(results)) {
            return null;
        }

        // loop objects until a cuntry code is found
        for (const result of results) {
            if (!helpers.checkIfPropertyIsArray(result.address_components)) {
                continue;
            }

            const foundType = result.address_components.find(address => {
                if (helpers.checkIfPropertyIsArray(address.types)) {
                    return address.types.find(typeList => typeList === constants.CONST_COUNTRY);
                }
            });

            if (!foundType) {
                continue;
            }

            const { long_name, short_name } = foundType;
            return {
                countryName: long_name,
                countryCode: short_name
            };
        }

        return null;
    },
    obtainCountryCapitalData: ({ countryCode }) => {
        const foundCountry = countryCapitals.find(country => country.CountryCode === countryCode);
        const { CountryName, CapitalName, CapitalLatitude, CapitalLongitude } = foundCountry || {};

        return !foundCountry ? null : {
            countryName: CountryName,
            capitalName: CapitalName,
            capitalLatitude: CapitalLatitude,
            capitalLongitude: CapitalLongitude
        };
    },
    obtainLocationSeason: (latitude) => {
        const { SEASON_SUMMER, SEASON_AUTUMN, SEASON_SPRING, SEASON_WINTER } = constants;
        const northernHemisphere = [SEASON_WINTER, SEASON_SPRING, SEASON_SUMMER, SEASON_AUTUMN];
        const southernHemisphere = [SEASON_SUMMER, SEASON_AUTUMN, SEASON_WINTER, SEASON_SPRING];
        const getSeason = d => Math.floor((d.getMonth() / 12 * 4)) % 4;
        
        const date = new Date();

        if (latitude > 0)
            return northernHemisphere[getSeason(date)];
        else
            return southernHemisphere[getSeason(date)];
    },
};
