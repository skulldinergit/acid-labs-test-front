import constants from './constants';

export default weatherServices = {
    getWeather: async({latitude, longitude}) => {
        try {
            // const url = `https://api.darksky.net/forecast/726f990087905ada4dfba3550cbee0b0/${latitude},${longitude}?lang=es&units=si`;
            const url = `${constants.URI_WEATHER}/weather?latitude=${latitude}&longitude=${longitude}`;
            console.log(url);

            let response = await fetch(url);
            
            if (response.ok){
                return await response.json();
            } else {
                return null;
            }
        } catch (error){
            console.log("error: " + error);
            return null;
        }
    },
};
