import constants from './constants';
import { mapHelper } from './helpers';

export default mapServices = {
    getLatlngData: async({latitude, longitude}) => {
        try {
            const response = await fetch(`https://maps.googleapis.com/maps/api/geocode/json?latlng=${latitude},${longitude}&sensor=true&key=${constants.MAP_API_KEY}`);
            
            if (response.ok){
                const responseJson =  await response.json();

                // proccess server response to obtain country data
                const countryData = mapHelper.obtainCountryData(responseJson); 

                if (!countryData){
                    return null;
                }

                // Try to obtain capital city matching country code
                const capitalData = mapHelper.obtainCountryCapitalData({ countryCode: countryData.countryCode });

                // Obtain capital station
                const capitalSeason = mapHelper.obtainLocationSeason(capitalData.capitalLatitude);

                return {
                    ...capitalData,
                    capitalSeason,
                };
            } else {
                return null;
            }
        } catch (error){
            // console.log("error: " + error);
            return null;
        }
    },
};
