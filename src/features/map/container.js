import React from 'react';
import { ToastAndroid } from 'react-native';
import { Marker, } from 'react-native-maps';
import { MapView } from 'expo';
import { connect } from 'react-redux';
import actionCreators from '../../store/actions';
import styles from './styles';

class MapContainer extends React.Component {
  state = {
    region: {
      latitude: -33.4691,
      longitude: -70.642,
      latitudeDelta: 0.0922,
      longitudeDelta: 0.0421,
    },
    selection: null
  }

  onMapPress = ({coordinate, position}) => {
    this.setState({
      selection: {
        latitude: coordinate.latitude,
        longitude: coordinate.longitude
      }
    });
  }

  onMarkerPress = async() => {
    const { latitude, longitude } = this.state.selection;

    // ToastAndroid.show(`Marker presed ${latitude}`, ToastAndroid.SHORT);
    await this.props.dispatch(actionCreators.openModal({ latitude, longitude }));
  }
  
  onDragEnd = ({coordinate}) => {
    this.setState({
      selection: {
        latitude: coordinate.latitude,
        longitude: coordinate.longitude
      }
    });
  }

  render = () => {
    const { loadingMap, countryData} = this.props;
    const { countryName } = countryData || {};

    const { region, selection } = this.state;
    const { latitude, longitude, latitudeDelta, longitudeDelta } = region;
    
    return (
      <MapView
        style={{ flex: 1 }}
        initialRegion={{
          latitude: latitude,
          longitude: longitude,
          latitudeDelta: latitudeDelta,
          longitudeDelta: longitudeDelta
        }}
        zoomEnabled={false}
        maxZoomLevel={3}
        showsUserLocation={true}
        onPress={(e) => this.onMapPress(e.nativeEvent)}
      >
        {selection && 
        <Marker
          ref={marker => { this.marker = marker }}
          draggable
          coordinate={{latitude: selection.latitude, longitude: selection.longitude}}
          onDragEnd={(e)=> this.onDragEnd(e.nativeEvent)}
          onDrag={() => this.marker.hideCallout()}
          onPress={(e) => this.onMarkerPress(e.nativeEvent)}
        />
        }
      </MapView>
    );
  }
}

const mapStateToProps = ({ store }) => {
    const { loadingMap, countryData } = store;

    return {
      loadingMap,
      countryData
    };
};

export default connect(mapStateToProps)(MapContainer);
