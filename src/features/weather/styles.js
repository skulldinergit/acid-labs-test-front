import { StyleSheet } from 'react-native';

export default styles = StyleSheet.create({
    country: {
        // flex: 1,
        flexDirection: 'row',
        marginHorizontal: 10,
        marginVertical: 10,
        backgroundColor: '#fff',
        alignItems: 'stretch',
        justifyContent: 'space-between',
        marginBottom: 10,
    },
    countryCaption: {
        fontSize: 18,
        fontWeight: 'bold',
    },
    countryValue: {
        fontSize: 18
    },
    season: {
        flexDirection: 'row',
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 10,
    },
    seasonCaption: {
        fontSize: 16,
        color: 'gray',
    },
    temperature: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    temperatureCaption: {
        fontSize: 40,
    },
    temperatureBlock: { 
        flexDirection: 'row',
         alignItems: 'center',
    },
    temperatureSummary: {
        marginVertical: 10,
        color: 'gray',
    }
});