import React from 'react';
import { View, } from 'react-native';
import { connect } from 'react-redux';
import { Text, Divider, } from 'react-native-elements';
import ModalComponent from '../../components/modal';
import actionCreators from '../../store/actions';
import WeatherIcon from './components/weatherIcon';
import WeatherImage from './components/weatherImage';
import WeatherList from './components/weatherList';
import styles from './styles';

class WeatherContainer extends React.Component {
    onBackDropPress = async () => {
        await this.props.dispatch(actionCreators.closeModal());
    }

    render() {
        const { showModal, latitude, longitude, loading, error, weatherData, countryData } = this.props;
        const { countryName, capitalName, capitalLatitude, capitalLongitude, capitalSeason } = countryData || {};
        const { currently, daily } = weatherData || {};

        return (
            <ModalComponent
                title={"Información meteorológica"}
                isVisible={showModal}
                onBackDropPress={() => this.onBackDropPress()}
                loading={loading}
                error={error}
            >
                <View style={styles.country}>
                    <Text style={styles.countryCaption}>País</Text>
                    <Text style={styles.countryValue}>{countryName}</Text>
                </View>
                <View style={styles.country}>
                    <Text style={styles.countryCaption}>Capital</Text>
                    <Text style={styles.countryValue}>{capitalName}</Text>
                </View>
                <View style={styles.season}>
                    <Text style={styles.seasonCaption}>{capitalSeason}</Text>
                    <WeatherIcon name={capitalSeason} />
                </View>
                <Divider />
                {currently !== undefined &&
                    <Text style={styles.temperatureSummary}>{currently.summary}</Text>
                }
                {currently !== undefined &&
                    <View style={styles.temperature}>
                        <View style={styles.temperatureBlock}>
                            <WeatherImage name={currently.icon} />
                            <Text style={styles.temperatureCaption}> {Math.round(currently.temperature)}°</Text>
                        </View>
                        <View>
                            <Text>Precip: {Math.round(currently.precipProbability * 100)}%</Text>
                            <Text>Humedad: {Math.round(currently.humidity * 100)}%</Text>
                            <Text>Viento: {currently.windSpeed} m/s</Text>

                        </View>
                    </View>
                }
                {daily !== undefined &&
                    <WeatherList 
                        data={daily.data}
                    />
                }
            </ModalComponent>
        );
    }
}

const mapStateToProps = ({ store }) => {
    const { showModal, latitude, longitude, loading, error, weatherData, countryData } = store;

    return {
        showModal,
        latitude,
        longitude,
        loading,
        error,
        weatherData,
        countryData
    };
};

export default connect(mapStateToProps)(WeatherContainer);