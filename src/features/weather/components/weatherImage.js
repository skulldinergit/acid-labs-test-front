import React from 'react';
import { Image } from 'react-native-elements';
import * as weatherImages from '../../../../assets/image/index';
import constants from '../constants';

const { 
    CLEAR_DAY_CAPTION,
    CLEAR_NIGHT_CAPTION,
    RAIN_ICON_CAPTION,
    SNOW_ICON_CAPTION,
    SLEET_CAPTION,
    WIND_CAPTION,
    FOG_CAPTION,
    CLOUDY_CAPTION,
    PARTLY_CLOUDY_DAY_CAPTION,
    PARTLY_CLOUDY_NIGHT_CAPTION
} = constants;

const {
    clearDay,
    clearNight,
    cloudy,
    fog,
    partlyCloudyDay,
    partlyCloudyNight,
    rain,
    sleet,
    snow,
    wind,
} = weatherImages;

const imageMap = [
    { name: CLEAR_DAY_CAPTION, image: clearDay  },
    { name: CLEAR_NIGHT_CAPTION, image: clearNight  },
    { name: RAIN_ICON_CAPTION, image: rain  },
    { name: SNOW_ICON_CAPTION, image: snow  },
    { name: SLEET_CAPTION, image: sleet  },
    { name: WIND_CAPTION, image: wind  },
    { name: FOG_CAPTION, image: fog  },
    { name: CLOUDY_CAPTION, image: cloudy  },
    { name: PARTLY_CLOUDY_DAY_CAPTION, image: partlyCloudyDay  },
    { name: PARTLY_CLOUDY_NIGHT_CAPTION, image: partlyCloudyNight  },
]

export default WeatherImage = (props) => {
    const { name, style } = props;
    const showImage = imageMap.find( cap => cap.name === name );

    const useStyle = style ? style : { width: 80, height: 80 }

    return (
        <Image
            source={showImage.image}
            style={useStyle}
        />
    );
};