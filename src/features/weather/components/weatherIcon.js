import React from 'react';
import { Icon, Text } from 'react-native-elements';
import constants from '../constants';
import apiConstants from '../../../api/constants';

const iconColors = [
    { name: apiConstants.SEASON_SUMMER, iconName: constants.SUMMER_ICON_NAME, color: '#FFBE00'},
    { name: apiConstants.SEASON_AUTUMN, iconName: constants.AUTUMN_ICON_NAME, color: '#FF5925'},
    { name: apiConstants.SEASON_WINTER, iconName: constants.WINTER_ICON_NAME, color: '#00AEFF'},
    { name: apiConstants.SEASON_SPRING, iconName: constants.SPRING_ICON_NAME, color: '#B4DF38'},
]

export default WeatherIcon = (props) => {
    const { name } = props;

    const selectedIcon = iconColors.find( icon => icon.name === name );

    return (
        selectedIcon !== undefined ? <Icon
            raised
            type='ionicon'
            name={selectedIcon.iconName}
            color={selectedIcon.color}
        /> :
        <Text>Error Icon</Text>
    );
};
