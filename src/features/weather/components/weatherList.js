import React from 'react';
import moment from 'moment/min/moment-with-locales';
import { FlatList, View, StyleSheet } from 'react-native';
import { Text } from 'react-native-elements';
import WeatherImage from './weatherImage';

moment.locale('es');

renderItem = (item) => {
    try {
        const { icon, time, temperatureMax, temperatureMin } = item.item;
        return (
            <View style={styles.component}>
                <Text style={styles.dayString}>{ String(moment.unix(time).format('ddd')).toUpperCase()}</Text>
                <WeatherImage name={icon} style={styles.icon}/>
                <Text style={styles.temperatureMax}>{Math.round(temperatureMax)}°</Text>
                <Text>{Math.round(temperatureMin)}°</Text>
            </View>
        );
    } catch (error) {
        return null;
    }
}

export default WeatherList = ({data}) => {
    return (
        <FlatList
            style={styles.container}
            horizontal
            data={data}
            keyExtractor={(item, index) => String(item.time)}
            renderItem={(item) => renderItem(item)}
        />
    )
};

const styles = StyleSheet.create({
    container: {
        marginTop: 30,
    },
    component: {
        flexDirection: 'column',
        // justifyContent: 'center',
        alignItems: 'center',
        width: 70,
        height: 150,
    },  
    dayString: {
        fontSize: 15,
        color: 'gray',
        fontWeight: 'bold',
        marginBottom: 10,
    },
    icon: {
        width: 20, 
        height: 20,
        marginBottom: 10,
    },
    temperatureMax: {
        marginBottom: 5,
        fontWeight: 'bold',
    }
});