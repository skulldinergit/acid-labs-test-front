export const types = {
    SET_INITIAL_STATE_WEATHER: "setInitialStateWeather",
    CLOSE_MODAL: "closeModal",
    OPEN_MODAL: "openModal",
    REQUEST_FINISH: "requestFinish",
    GET_COUNTRY_DATA: "getCountryData",
    GET_WEATHER_DATA: "getWeatherData",
    REQUEST_ERROR: "requestError",
};