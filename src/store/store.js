import { createStore, applyMiddleware } from 'redux';
import ReduxThunk from 'redux-thunk';
import combineReducers from './reducers';

const store = createStore(combineReducers, {}, applyMiddleware(ReduxThunk));
export default store;