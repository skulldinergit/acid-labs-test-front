import { types } from './constants';
import { combineReducers } from "redux";

const INITIAL_STATE = {
    city: null,
    weatherData: null,
    countryData: null,
    latitude: null,
    longitude: null,
    loading: true,
    loadingMap: true,
    error: null,
    showModal: false,
};


const storeReducer = (state = INITIAL_STATE, {type, payload, error}) => {
    switch (type) {
        case types.SET_INITIAL_STATE_WEATHER:
            return INITIAL_STATE;
        case types.CLOSE_MODAL:
            return {
                ...state,
                loading: false,
                showModal: false
            };
        case types.OPEN_MODAL:
            return {
                ...state,
                loading: true,
                loadingMap:true,
                showModal:true,
                error: null,
                latitude: payload.latitude,
                longitude: payload.longitude
            };
        case types.REQUEST_FINISH:
            return {
                ...state,
                loading:false,
                weatherData: payload
            };
        case types.GET_COUNTRY_DATA:
            return {
                ...state,
                loading: false,
                loadingMap:false,
                countryData: {...payload}
            };
        case types.GET_WEATHER_DATA:
            return {
                ...state,
                loading: false,
                loadingMap:false,
                countryData: { ...payload.mapData },
                weatherData: { ...payload.weatherData}
                
            };
        case types.REQUEST_ERROR:
            return {
                ...state,
                loading: false,
                countryData: null,
                weatherData: null,
                error: payload.error
            };
    }
    return state;
};

export default combineReducers({
    store: storeReducer
});