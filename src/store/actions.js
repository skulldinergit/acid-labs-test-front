import { types } from './constants';
import weatherServices from '../api/weatherServices';
import mapServices from '../api/mapServices';

export default actionCreators = {
    setInitialStateWeather: () => async (dispatch) => {
        return await dispatch({ type: types.SET_INITIAL_STATE_WEATHER });
    },
    closeModal: () => async (dispatch) => {
        return await dispatch({ type: types.CLOSE_MODAL });
    },
    openModal: ({ latitude, longitude }) => async (dispatch) => {
        await dispatch({
            type: types.OPEN_MODAL,
            payload: { latitude, longitude }
        });

        // get the requst from server
        const mapData = await mapServices.getLatlngData({ latitude, longitude });

        if (!mapData) {
            return await dispatch({
                type: types.REQUEST_ERROR,
                payload: { error: 'Error en el request a la API de Google Maps' }
            });
        }

        const { capitalLatitude, capitalLongitude } = mapData;
        // Get data from weather service for capital city coords
        const weatherData = await weatherServices.getWeather({ latitude: capitalLatitude, longitude: capitalLongitude });
        
        if (!weatherData) {
            return await dispatch({
                type: types.REQUEST_ERROR,
                payload: { error: 'Error en el requesta a la API de DarkSky'}
            });
        }

        return await dispatch({
            type: types.GET_WEATHER_DATA,
            payload: { mapData, weatherData: weatherData.data, }
        })
    }
}