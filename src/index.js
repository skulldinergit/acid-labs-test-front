import React, { Component } from "react";
import { Provider } from "react-redux";
import { Header } from 'react-native-elements';
import store from "./store/store";
import MapContainer from './features/map/container';
import WeatherContainer from './features/weather/container'

export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Header 
          backgroundColor='#FF7043'
          centerComponent={{ text: 'Acid Labs Test RN', style: { color: '#fff' } }}
        />
        <MapContainer />
        <WeatherContainer/>
      </Provider>
    );
  }
}
