# acid-labs-test-front

React native app for the acid labs test front end part

# Expo
- La URL para ver la app online es ```https://expo.io/@crisskulld/acid-labs-test-frontend```

# Funcionamiento
- Inicia en una pantalla con el google map.
- Para seleccionar una posición, puede hacer click en un punto o drag and drop. Esto solo mueve el cursor.
- Para ver el modal de clima de la capital, debe hacer click sobre un marker.

# Scripts
- Para iniciar el expo bundler, ocupar ```npm run start```

# Servidor remoto
- El servidor remoto de la app en heroku es ```https://sheltered-badlands-78307.herokuapp.com/```